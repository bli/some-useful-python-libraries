# Some useful Python libraries

The current plan is to use this repository to list (and possibly demonstrate the use of) some Python libraries I use, have used or may want to use on day.

## TODO

* Complete the list
* Add citations
* Add Jupyter notebooks to demonstrate usage


## General

### cytoolz

Lots of useful functions for functional programming fun, hopefully faster than pytoolz.

The documentation is that of pytoolz: <https://toolz.readthedocs.io/en/latest/>

URL: <https://pypi.org/project/cytoolz/>

### dask

I once tried a few stuff from this library to run things in parallel.

URL: <https://www.dask.org/>

### snakemake

To build workflows, with versatility provided by Python.

My advice: don't use `expand`, but standard Python constructs instead.

Using input functions, it is possible to assemble quite complex workflows.

URL: <https://snakemake.readthedocs.io/en/stable/>

### nimpy

To build Python modules using Nim.
I haven't used it yet.

URL: <https://github.com/yglukhov/nimpy>

### codon

To compile code written in some restricted version of the Python language into
hopefully something more efficient than a Python script.

I haven't tried it.

URL: <https://docs.exaloop.io/codon>

### beautifulsoup

To parse XML formats.

URL: <https://www.crummy.com/software/BeautifulSoup/>


## Graphics

### matplotlib

Base graphics library.

URL: <https://matplotlib.org/>

### seaborn

Built on top of matplotlib, to provide some common graphical representations.

URL: <https://seaborn.pydata.org/>

### plotnine

One of the Python "versions" of ggplot.

URL: <https://plotnine.readthedocs.io/en/stable/index.html>

### pylustrator

To manually adjust matplotlib figures and get the corresponding code.
I haven't tried it.

URL: <https://pylustrator.readthedocs.io/en/latest/>


## General data analysis

### scipy

I sometimes use the `stats` module, and otherwise in combination with other libraries that depend on scipy.

URL: <https://docs.scipy.org/doc/scipy/index.html>

### numpy

Like with scipy, I usually use it in combination with other libraries that depend on it.

URL: <https://numpy.org/>

### pandas

Provides lots of ways to handle tabular data.

URL: <https://pandas.pydata.org/>

### scikit-learn

General machine learning.

URL: <https://scikit-learn.org/stable/>

### plydata

"dplyr" for pandas.

<https://plydata.readthedocs.io/en/stable/index.html>

### networkx

Useful for some optimisation problems that can be modelled using graphs.

URL: <https://networkx.org/>

### polars

Promises efficient data frames to use instead of pandas.
I haven't tried it yet as of September 2024.

URL: <https://www.pola.rs/>

### pyarrow

Library to use the highly efficient Apache Parquet file format from scipy and pandas.
I haven't tried it yet as of November 2024.

URL: <https://arrow.apache.org/docs/python/index.html>

### anndata

Another alternative for pandas?
I haven't tried it.

URL: <https://anndata.readthedocs.io/en/latest/>

### rpy2

I have used it to run functions provided by R packages from Python because programming with R was frustrating.
However, using rpy2 was not really a pleasant programming experience either.

URL: <https://rpy2.github.io/>


## Bioinformatics

### biopython

I actually rarely use it, but this is a reference in terms of handling biology-related data.

URL: <https://biopython.org/>

### pysam

I use it to parse bam of vcf files.

URL: <https://pysam.readthedocs.io/en/latest/api.html>

### cyvcf2

I plan to try it to parse vcf files.

URL: <https://brentp.github.io/cyvcf2/index.html>

### mappy

I use its fast fasta / fastq parser:

```{python}
from mappy import fastx_read

for (name, seq, qual) in fastx_read("path_to_data.fastq"):
    pass
```

URL: <https://pypi.org/project/mappy/>

### gffutils

To work with genome annotations.

URL: <https://daler.github.io/gffutils/>

### pyranges

To work with genome annotations.

URL: <https://pyranges.readthedocs.io/>

### pybedtools

Wrapper around bedtools, to work with genomic intervals.

URL: <https://daler.github.io/pybedtools/>

### deeptools

Various tools to display "deep" sequencing data. There is a Python API.

URL: <https://deeptools.readthedocs.io/en/develop/index.html>

### pybigwig

To handle bigwig files.

URL: <https://github.com/deeptools/pyBigWig>

### biotite

I used some of the tools it provides to display sequences, with colours:
<https://www.biotite-python.org/apidoc/biotite.sequence.graphics.html#module-biotite.sequence.graphics>

URL: <https://www.biotite-python.org/>

### parasail

To perform pairwise sequence alignment.

URL: <https://github.com/jeffdaily/parasail-python>

### scikit-bio

I have used it to handle distance matrices: <http://scikit-bio.org/docs/latest/generated/skbio.stats.distance.html>

URL: <http://scikit-bio.org/>

### scikit-allel

For population genetics.

URL: <https://scikit-allel.readthedocs.io/en/stable/index.html>


## Phylogenetics

### ete

To display phylogenetic trees. I briefly used it a long time ago.

URL: <http://etetoolkit.org/>

### dendropy

To manipulate phylogenetic trees. I briefly used it a long time ago.

URL: <https://dendropy.org/>

### p4

Versatile MCMC phylogeny building using Python.

URL: <https://p4.nhm.ac.uk/>

### tskit

URL: <https://tskit.dev/genetics-research/>

To work with "succint tree sequences", an efficient genomic data storage format
supposedly useful for population genetics and phylogeny. I haven't used it yet.


## Not a library

### Software Carpentry

I used some of their old material (before 2010) to improve my programming practice in Python.

URL: <https://software-carpentry.org/lessons/>

### Python charts

A guide to data visualisation using Python.

URL: <https://python-charts.com/>

### Jupytext

A way to store jupyter notebooks in a more human-readable format (without
outputs), possibly paired with standard .ipynb notebook.
As of September 2024, I haven't tried it.

URL: <https://github.com/mwouts/jupytext>

### The little books of Python anti-patterns

Some programming good practice.

URL: <https://quantifiedcode.github.io/python-anti-patterns/>

### FIDLE

Introduction to deep learning, with an admirable course support based on Jupyter notebooks.

URL: <https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle>
